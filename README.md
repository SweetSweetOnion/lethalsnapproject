# LETHAL SNAP
![icon](https://i.imgur.com/g8enPXU.png)

Add a polaroid to Lethal Company!\
**Take pictures** of the worst moment of your expeditions and try to **sell them** to the evil company.\
Picture gain values based on the monster type.\
You can keep your favorite pictures in your ship and **pin them** to any surfaces.

# Some examples

![TeamPictures](https://i.imgur.com/AkNj4MI.png)
![AlbumCover](https://i.imgur.com/44oQY84.png)
![ABeautifulShip](https://i.imgur.com/Jbcmx3c.png)
![PrettyRabbit](https://i.imgur.com/ifxeoCT.png)
![Run](https://i.imgur.com/fqnId3V.png)
![TeamPicture2](https://i.imgur.com/WYYqpsS.png)

# Important information
All pictures are saved in the folder: *path-to-the-game\LethalCompany\PolaroidPictures*\
Currently they are never removed (might change in future update)

Config appear here after first launch: *BepixEx/config/LethalSnapProject.cfg*\
**To get the last balancing update, if it's not your first launch, remove the config file before launch (or copy the default value after launching once)**
(in thunderstore you can go into your profile > edit config > BepinEX/config/LethalSnapProject.cfg to find it)

# Next Step
- Coil head don't move when watching a picture of him
- Scopophobia is triggered when holding a picture of him
- Auto remove picture after a maximum (100 photo ?) or when the save is erased
- Add randomness in the description of the picture


# Credits
An awesome team: 
- SweetOnion (Code and 3d) [itch.io](https://sweetonion.itch.io/) 
- Balroguette (2d art and playtest) [@Balroguette_betise](https://www.instagram.com/balroguette_betise) 
- Apoal (Audio and playtest)
- Telios (Doc and playtest)
- Rambar (Playtest)
- Valemen (Playtest)
- Fab (Playtest and Prog 3d)


