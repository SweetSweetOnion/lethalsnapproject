﻿using UnityEngine;


namespace PolaroidMod
{
	internal class PolaroidItem : PhysicsProp
	{

		public override void ItemActivate(bool used, bool buttonDown = true)
		{
			base.ItemActivate(used, buttonDown);
			if (buttonDown)
			{
				if(playerHeldBy != null)
				{
					playerHeldBy.DamagePlayer(20);
				}
				else
				{
					Debug.Log("Active polaroid");
				}
			}
		}
	}
}
