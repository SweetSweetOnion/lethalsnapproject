﻿using UnityEngine;
using UnityEngine.UI;
using System.IO;
using Unity.Netcode;
using System.Collections.Generic;
using UnityEngine.UIElements;
using System;
using System.Linq;
using GameNetcodeStuff;

namespace LethalSnapProject.Behaviour
{
	internal class PictureItem : GrabbableObject
	{
		public int pictureID = 0;
		public bool isPin = false;
		public Vector3 pinPosition;
		public Quaternion pinRotation;
		public string description;
		public LayerMask layer;

		public bool spawnFromSaved = false;

		public Texture2D pictureTex = null;
		public string pictureMetadataID = "";
		private ScanNodeProperties scanNodeProperties;

		private NetworkVariable<int> _PictureID = new NetworkVariable<int>();
		private NetworkVariable<bool> _SpawnFromSaved = new NetworkVariable<bool>();

		private AudioSource source;

		public int PictureID { get { return _PictureID.Value; } }
	

		public static List<string> picturesMetaDataId = new List<string>();

		private Transform pictureMesh;
		private Vector3 pictureMeshStartScale;

		private BoxCollider collider;
		private Vector3 boxColliderStartSize;


		public string[] layers = new string[]
		{
			"Default",	
			//"Props",
			"Room",
			"InteractableObject",
			"Colliders",
			"PhysicsObjects",
			"MiscLevelGeometry",
			"Terrain",
			//"PlaceableShipObjects",
			"PlacementBlocker",
			"Railing",
			"DecalStickableSurface"
		};

		public string[] banParents = new string[]
		{
			"SteelDoor (1)",
			"LevelGeneration",
			"EntranceTeleportA(Clone)",
			"VolumeMain (1)"
		};

		private void CustomStart()
		{
			propColliders = base.gameObject.GetComponentsInChildren<Collider>();
			originalScale = base.transform.localScale;
			
			if (itemProperties.isScrap && RoundManager.Instance.mapPropsContainer != null)
			{
				radarIcon = UnityEngine.Object.Instantiate(StartOfRound.Instance.itemRadarIconPrefab, RoundManager.Instance.mapPropsContainer.transform).transform;
			}

			if (!itemProperties.isScrap)
			{
				HoarderBugAI.grabbableObjectsInMap.Add(base.gameObject);
			}

			MeshRenderer[] componentsInChildren = base.gameObject.GetComponentsInChildren<MeshRenderer>();
			for (int i = 0; i < componentsInChildren.Length; i++)
			{
				componentsInChildren[i].renderingLayerMask = 1u;
			}

			SkinnedMeshRenderer[] componentsInChildren2 = base.gameObject.GetComponentsInChildren<SkinnedMeshRenderer>();
			for (int j = 0; j < componentsInChildren2.Length; j++)
			{
				componentsInChildren2[j].renderingLayerMask = 1u;
			}
		}

		public override void Start()
		{			
			source = GetComponent<AudioSource>();
			layer = LayerMask.GetMask(layers);

			pictureMesh = transform.Find("Mesh");
			pictureMeshStartScale = pictureMesh.localScale * CustomConfig.pictureScaleMultiplier.Value;

			collider = GetComponent<BoxCollider>();
			boxColliderStartSize = collider.size;

			if (IsHost || IsServer)
			{
				_PictureID.Value = pictureID;
				_SpawnFromSaved.Value = spawnFromSaved;
			}
			else
			{
				spawnFromSaved = _SpawnFromSaved.Value;			
			}	

			if (spawnFromSaved)
			{
				if (IsHost || IsServer)
				{
					UpdatePinClientRpc(isPin, pinPosition, pinRotation, Helper.GetMountPath(transform));
					SyncPictureDataClientRpc(description, scrapValue, "OLD" + pictureID);
				}
				else
				{
					SyncSaveStartServerRpc();
				}
			}
			else
			{
				if (IsHost || IsServer)
				{
					Plugin.MyLogger.LogInfo("Sync picture data : " + scanNodeProperties.headerText);
					SyncPictureDataClientRpc(scanNodeProperties.headerText, scrapValue, pictureMetadataID);
				}
			}

			if (isPin)
			{
				CustomStart();
			}
			else
			{
				base.Start();
			}

		}

		private bool canPinPicture = false;

		public override void ItemActivate(bool used, bool buttonDown = true)
		{
			base.ItemActivate(used, buttonDown);

			if (playerHeldBy != null && IsOwner)
			{
				canPinPicture = true;
			}
		}

		public override void LateUpdate()
		{
			base.LateUpdate();

			
			if (playerHeldBy != null && isHeld)
			{
				RaycastHit myHit;
				if (Physics.Raycast(playerHeldBy.gameplayCamera.transform.position, playerHeldBy.gameplayCamera.transform.forward, out myHit, 2, layer))
				{
					if(myHit.transform == null)
					{
						//Plugin.MyLogger.LogError("No transform " + myHit.collider + "   "  + myHit.distance);
					}
					else
					{
						//Plugin.MyLogger.LogWarning(myHit.transform.name + "   " + LayerMask.LayerToName(myHit.transform.gameObject.layer));

						if (myHit.transform == playerHeldBy.transform) return;

						if(!CustomConfig.canPinPictureToPlayers.Value && myHit.transform.GetComponent<PlayerControllerB>() != null)
						{
							return;
						}

						


						if (canPinPicture)
						{
							Plugin.MyLogger.LogInfo("Trying to pin picture to: " + myHit.transform.name + " On layer: " + LayerMask.LayerToName(myHit.transform.gameObject.layer));

							if (Plugin.DebugMode)
							{
								Helper.PrintLayerMask(layer,"Picture pin layerMask");
							}

							canPinPicture = false;
							Vector3 pos = myHit.point;
							Quaternion rot = Quaternion.identity;

							float d = Mathf.Abs(Vector3.Dot(myHit.normal, Vector3.up));
							rot.SetLookRotation(-Vector3.up, myHit.normal);						
								
							playerHeldBy.DiscardHeldObject(true, null, myHit.point);

							string rootPath = Helper.FindFirstTransformFromMountPath(Helper.GetMountPath(myHit.transform));

							if (IsBanParent(rootPath) || IsBanParent(myHit.transform.name))
							{
								transform.parent = null;
								Plugin.MyLogger.LogInfo("Pin target root: " + rootPath +" or pin target parent: " + myHit.transform.name + " are banned !  Pinning picture in world position");
							}
							else
							{
								transform.parent = myHit.transform;
								
							}

							if (transform.parent)
							{
								Plugin.MyLogger.LogInfo("Pin target to: " + transform.parent.name);
								pos = transform.parent.InverseTransformPoint(pos);
								rot = Quaternion.Inverse(rot) * transform.parent.rotation;
							}



							if (IsHost || IsServer)
							{
								UpdatePinClientRpc(true, pos, rot, Helper.GetMountPath(transform));
							}
							else
							{
								UpdatePinServerRpc(true, pos, rot, Helper.GetMountPath(transform));
							}
						}

					}
				}
				else
				{
					canPinPicture = false;
				}
			}

			

			if (isPin)
			{
				transform.localPosition = pinPosition;
				transform.localRotation = pinRotation;
			}

			transform.localScale = Vector3.one;
			UpdateScale();
			

		}

		private void UpdateScale()
		{
			Vector3 ratio = new Vector3(1, 1, 1);
			Transform parent = pictureMesh.parent;
			pictureMesh.parent = null;
			pictureMesh.localScale = pictureMeshStartScale;
			Vector3 prevScale = pictureMesh.localScale;
			pictureMesh.parent = parent;

			ratio = new Vector3(pictureMesh.localScale.x / prevScale.x, pictureMesh.localScale.y / prevScale.y, pictureMesh.localScale.z / prevScale.z);
			
			collider.size = new Vector3(boxColliderStartSize.x * ratio.x, boxColliderStartSize.y * ratio.y, boxColliderStartSize.z * ratio.z) * CustomConfig.pictureScaleMultiplier.Value;
		}

		private bool IsBanParent(string str)
		{
			for (int i = 0; i < banParents.Length; i++)
			{
				if(str == banParents[i])
				{
					return true;
				}
			}
			return false;
		}

		private void UnPin()
		{
			if (IsHost || IsServer)
			{
				UpdatePinClientRpc(false, Vector3.zero, Quaternion.identity,"");
			}
			else
			{
				UpdatePinServerRpc(false, Vector3.zero, Quaternion.identity,"");
			}
		}

		public override void DiscardItem()
		{
			base.DiscardItem();
			UnPin();
		}

		[ServerRpc]
		public void UpdatePinServerRpc(bool setPin, Vector3 pinPos, Quaternion pinRot, string mountPath)
		{
			UpdatePinClientRpc(setPin, pinPos, pinRot, mountPath);
		}

		[ClientRpc]
		public void UpdatePinClientRpc(bool setPin, Vector3 pinPos, Quaternion pinRot, string mountPath)
		{

			if (isPin && !setPin)
			{
				transform.parent = null;
			}
			if (setPin)
			{
				transform.parent = Helper.FindTransformFromMountPath(mountPath);
			}
			isPin = setPin;
			pinPosition = pinPos;
			pinRotation = pinRot;
			Plugin.MyLogger.LogInfo("Client RPC pin : " + setPin + "  " + pinPos);
		}

		public override void GrabItem()
		{
			base.GrabItem();
			UnPin();
			source.PlayOneShot(Plugin.AudioPictureGrab, 1);
		}

		public override void OnHitGround()
		{
			base.OnHitGround();
			source.PlayOneShot(Plugin.AudioPictureDrop, 1);

		}

		public override void EquipItem()
		{
			base.EquipItem();
			source.PlayOneShot(Plugin.AudioPictureEquip, 1);
		}

		public override void OnNetworkSpawn()
		{
			base.OnNetworkSpawn();
			scanNodeProperties = GetComponentInChildren<ScanNodeProperties>();
			_PictureID.OnValueChanged += OnPictureIDChange;
			if (!IsHost && !IsServer)
			{
				InitPicture();
			}

		}
		public override void OnNetworkDespawn()
		{
			base.OnNetworkDespawn();
			_PictureID.OnValueChanged -= OnPictureIDChange;
		}

		public void RegisterPictureMetadata()
		{
			picturesMetaDataId.Add(pictureMetadataID);
		}

		public bool IsPictureUnique()
		{
			return !picturesMetaDataId.Contains(pictureMetadataID);
		}

		private void InitPicture()
		{
			PictureItem picture = GetComponent<PictureItem>();
			picture.pictureID = PictureID;
			picture.UpdatePicture();
		}

		private void OnPictureIDChange(int prevValue, int newValue)
		{
			PictureItem picture = GetComponent<PictureItem>();
			picture.pictureID = newValue;
			picture.UpdatePicture();
		}


		public bool UpdatePicture()
		{
			if (pictureID == 0) return false;
			pictureTex = GetSavedImage();
			if (pictureTex != null)
			{
				var rend = GetComponentInChildren<MeshRenderer>();
				var mat = rend.material;
				mat.mainTexture = pictureTex;
				mat.SetTexture("_EmissiveColorMap", pictureTex);
				return true;
			}
			return false;
		}

		

		public Texture2D GetSavedImage()
		{
			string filePath = PolaroidItem.GetDirPath() + pictureID + ".png";

			if (!File.Exists(filePath))
			{
				Plugin.MyLogger.LogWarning("No File found for " + filePath);
				return null;
			}
			Plugin.MyLogger.LogInfo("Found file at : " + filePath);
			byte[] bytes = File.ReadAllBytes(filePath);
			Texture2D tex = new Texture2D(1, 1);
			tex.LoadImage(bytes);
			return tex;
		}

		public override void LoadItemSaveData(int saveData)
		{
			base.LoadItemSaveData(saveData);

			if (IsClient) return; // return when called form client (always false on host)

			pictureID = saveData;
			
		
			SaveSystem.LoadAndUpdatePicture(this);

			pinPosition = transform.localPosition;
			pinRotation = transform.localRotation;

			if (Plugin.DebugMode)
			{
				Helper.SpawnDebugVisual(transform.position, Color.white);
			}

			spawnFromSaved = true;
		}

		public override void OnDestroy()
		{
			if (RoundManager.Instance.playersManager.firingPlayersCutsceneRunning || RoundManager.Instance.playersManager.allPlayersDead)
			{
				SaveSystem.RemoveImageFile(PictureID);
			}
			else
			{
				if (!isInShipRoom)
				{
					SaveSystem.RemoveImageFile(PictureID);
				}
			}
			
			base.OnDestroy();
		}

		public override int GetItemDataToSave()
		{
			if (IsHost || IsServer)
			{
				//Dirty stuff here ! (saving is actually perform only on the first picture save)
				SaveSystem.Save(FindObjectsOfType<PictureItem>().ToList());
				
			}
			picturesMetaDataId.Clear();
			return pictureID;
		}

		[ServerRpc(RequireOwnership = false)]
		private void SyncSaveStartServerRpc()
		{
			UpdatePinClientRpc(isPin, pinPosition, pinRotation, Helper.GetMountPath(transform));
			SyncPictureDataClientRpc(description, scrapValue, "OLD" + pictureID);
		}


		[ClientRpc]
		private void SyncPictureDataClientRpc(string header, int scrap, string metadata)
		{
			description = header;
			scrapValue = scrap;
			scanNodeProperties.headerText = description;		 
			SetScrapValue(scrapValue);
			itemProperties.creditsWorth = scrapValue;
			pictureMetadataID = metadata;
			MeshRenderer borderRenderer = transform.Find("Mesh").Find("Border").GetComponent<MeshRenderer>();

			if (scrapValue <= 0)
			{
				borderRenderer.material.color = Color.white;
				borderRenderer.material.SetColor("_EmissiveColor", Color.grey);

			}
			else
			{
				borderRenderer.material.color = Color.yellow;
				borderRenderer.material.SetColor("_EmissiveColor", Color.yellow);

			}

			Plugin.MyLogger.LogInfo("CLIENT sync picture node to: " + scanNodeProperties.subText + "  " + scrapValue);
		}
	}
}
