﻿using LethalLib;
using UnityEngine;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine.XR;


namespace LethalSnapProject.Behaviour
{
	static class SaveSystem
	{
		public const int delayBetweenLoad = 5;
		public static float lastLoad = 0;
		public const int delayBetweenSave = 5;
		public static float lastSave = 0;
		public static SaveContainer container;
		private static string GetSaveFileName()
		{
			var gnm = GameNetworkManager.Instance;
			if (!gnm)
			{
				Plugin.MyLogger.LogError("no network manager instance");
				return "";
			}
			return gnm.currentSaveFileName;
		}

		private static string GetSavePath()
		{
			return PolaroidItem.GetDirPath() + "" + GetSaveFileName() + ".json";
		}

		private static bool NeedReload()
		{
			if (lastLoad !=0 && Time.time < lastLoad + delayBetweenLoad)
			{
				return false;
			}
			return true;
		}

		private static bool TryGetJson(out string json)
		{
			json = "";
			if (!File.Exists(GetSavePath()))
			{
				Plugin.MyLogger.LogWarning("No save at : " + GetSavePath());
				return false;
			}

			json = File.ReadAllText(GetSavePath());
			return true;
		}


		private static void Load()
		{
			if(!NeedReload())
			{
				if(Plugin.DebugMode)Plugin.MyLogger.LogWarning("Abort reloading save. Use last loading instead");
				return;
			}
			lastLoad = Time.time;
			string json = "";
			if (!TryGetJson(out json))
			{
				container = null;
				return;
			}
			if (json == "") return;
			container = JsonUtility.FromJson<SaveContainer>(json);
		}

		public static void LoadAndUpdatePicture(PictureItem item)
		{
			Load();

			if(container == null)
			{
				Plugin.MyLogger.LogWarning("No container");
				return;
			}
			Plugin.MyLogger.LogInfo("Container loaded ! applying data to picture: " + item.description);
			Apply(item, ref container, item.pictureID);
		}

		public static void Save(List<PictureItem> pictures)
		{
			if (lastSave != 0 && Time.time < lastSave + delayBetweenSave)
			{
				if (Plugin.DebugMode) Plugin.MyLogger.LogWarning("Can't save ! not enough time with the previous save" + Time.time + "  " + lastSave);
				return;
			}
			lastSave = Time.time;
			
			SaveContainer saveContainer = new SaveContainer();

			Plugin.MyLogger.LogInfo("NUMBER OF PICTURE TO SAVE: " + pictures.Count);
			for(int i =0; i< pictures.Count; i++)
			{
				AddToSaveContainer(pictures[i], ref saveContainer);
			}
			string json = JsonUtility.ToJson(saveContainer);
			Plugin.MyLogger.LogInfo("Creating json data: " + json);
			WriteJson(json);
		}

		private static void WriteJson(string newJson)
		{
			var dirPath = PolaroidItem.GetDirPath();
			if (!Directory.Exists(dirPath))
			{
				Directory.CreateDirectory(dirPath);
			}
			string filePath = GetSavePath();

			if (!File.Exists(filePath))
			{
				File.WriteAllText(filePath, newJson);
				Plugin.MyLogger.LogInfo("Saving pictures data to " + filePath);
			}
			else
			{


				File.WriteAllText(filePath, newJson);
				Plugin.MyLogger.LogInfo("Replace previous picture save at " + filePath);
			}
		}

		public static void RemoveImageFile(int pictureID)
		{
			string filePath = PolaroidItem.GetDirPath() + pictureID + ".png";

			if (!File.Exists(filePath))
			{
				Plugin.MyLogger.LogWarning("No File found for " + filePath);
				return;
			}

			File.Delete(filePath);
			Plugin.MyLogger.LogInfo("!!! Delete picture at: " + filePath);
		}

		private static void AddToSaveContainer(PictureItem item, ref SaveContainer container)
		{
			container.pictureIDs.Add(item.pictureID);
			container.isPin.Add(item.isPin);
			container.description.Add(item.description);
			container.scrapValue.Add(item.scrapValue);
			container.pos.Add(item.transform.position);
			container.rot.Add(item.transform.rotation);

			Plugin.MyLogger.LogInfo("Saving picture " + item.description + " " + item.scrapValue);
		}

		private static void Apply(PictureItem item, ref SaveContainer container, int pictureID)
		{
			int index = container.GetPictureIndex(pictureID);
			if(index == -1)
			{
				Plugin.MyLogger.LogWarning("Didn't found save for : " + item.pictureID);
				return;
			}
			if(index < container.isPin.Count)
			{
				item.isPin = container.isPin[index];
			}			
			if (index < container.pos.Count)
			{
				item.transform.position = container.pos[index];
			}
			if (index < container.rot.Count)
			{
				item.transform.rotation = container.rot[index];
			}
			if (index < container.description.Count)
			{
				item.description = container.description[index];
			}
			if (index < container.scrapValue.Count)
			{
				item.scrapValue = container.scrapValue[index];
			}

			Plugin.MyLogger.LogInfo("Loading picture " + item.description + " " + item.scrapValue);
		}
	}


	[Serializable]
	public class SaveContainer {

		public List<int> pictureIDs = new List<int>();
		public List<bool> isPin = new List<bool>();
		public List<string> description = new List<string>();
		public List<int> scrapValue = new List<int>();
		public List<Vector3> pos = new List<Vector3>();
		public List<Quaternion> rot = new List<Quaternion>();

		public int GetPictureIndex(int pictureID)
		{
			if (pictureIDs == null) return -1;
			for(int i = 0; i < pictureIDs.Count; i++)
			{
				if (pictureIDs[i] == pictureID)
				{
					return i;
				}
			}
			return -1;
		}

		public void Reset()
		{
			pictureIDs.Clear();
			isPin.Clear();
			description.Clear();
			scrapValue.Clear();
			pos.Clear();
			rot.Clear();
		}
	}
}

