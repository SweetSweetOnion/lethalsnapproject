﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Unity.Netcode;
using UnityEngine.UI;

namespace LethalSnapProject.Behaviour
{
	internal static class PictureHelper
	{

		public struct MonsterNameValue
		{
			public string Name;
			public int Value;

			public MonsterNameValue(string n, int v)
			{
				Name = n;
				Value = v;
			}
		}

		public static List<MonsterNameValue> additionalMonsterValues = new List<MonsterNameValue>();

		public static string MakePictureDescription(List<VisibleEntity> entities, bool isUnique)
		{
			string pictureDescription = "";

			if (entities.Count == 0)
			{
				pictureDescription = "A boring picture with nothing in it";
				return pictureDescription;
			}
			else
			{
				if (isUnique)
				{
					pictureDescription = "A beautiful picture of ";
				}
				else
				{
					pictureDescription = "[Copy] A beautiful picture of ";
				}
				
			}

			for (int i = 0; i < entities.Count; i++)
			{
				if (!entities[i].isPlayer)
				{
					pictureDescription += "a ";
				}
				if (i == entities.Count - 1)
				{
					pictureDescription += entities[i].name + ".";
				}
				else if (i == entities.Count - 2)
				{
					pictureDescription += entities[i].name + " and ";
				}
				else
				{
					pictureDescription += entities[i].name + ", ";
				}
			}
			return pictureDescription;
		}


		public static int ComputePictureValue(List<VisibleEntity> entities, bool isUnique)
		{
			int playerVisible = 0;
			int monsterVisible = 0;
			float monsterScore = 0;
			int scrapValue = 0;

	
			for (int i = 0; i < entities.Count; i++)
			{
				if (entities[i].isPlayer)
				{
					playerVisible++;
				}
				else
				{
					
					monsterVisible++;
					monsterScore += GetMonsterScore(entities[i].name);
					
				}
			}

			if (!isUnique)
			{
				Plugin.MyLogger.LogInfo("This picture already exists ! Set scrap value to 0");
				scrapValue = 0;
			}
			else
			{
				scrapValue = (int)monsterScore;
				Plugin.MyLogger.LogInfo("Picture has " + scrapValue + " value");
			}
			
			return scrapValue;
		}

		private static float GetMonsterScore(string monsterName)
		{		
			string lowerMonsterName = monsterName.ToLower();
			float? monsterValue = GetScrapFromAdditionalMonster(lowerMonsterName);

			if(monsterValue == null)
			{	
				monsterValue = CustomConfig.defaultMonsterValue.Value;
				Plugin.MyLogger.LogError("UNKNOWN MONSTER! Add " + monsterName + "to the config files to add a specific value! The default value is currently applied");				
			}
			Plugin.MyLogger.LogInfo("You catch: " + monsterName + " with a value of  " + monsterValue);

			return monsterValue.Value * CustomConfig.monsterValueMultiplier.Value;
		}

		private static float? GetScrapFromAdditionalMonster(string monsterName)
		{
			float? value = null;

			foreach(MonsterNameValue m in additionalMonsterValues)
			{
				if(monsterName == m.Name)
				{
					value = m.Value;
				}
			}

			return value;
		}

	}
}
