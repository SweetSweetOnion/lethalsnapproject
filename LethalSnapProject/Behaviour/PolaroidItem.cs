﻿using System.Collections;
using UnityEngine.Rendering.HighDefinition;
using UnityEngine;
using Unity.Netcode;
using System.IO;
using System.Threading.Tasks;
using GameNetcodeStuff;
using System.Collections.Generic;
using System;
using System.Net.NetworkInformation;
using System.Runtime.CompilerServices;
using LethalSnapProject.Patcher;

namespace LethalSnapProject.Behaviour
{
	internal class PolaroidItem : GrabbableObject
	{

		private IEnumerator pictureCoroutine;

		private Camera cam;
		private MeshRenderer screen;
		private MeshRenderer noBatteryRend;
		private Material screenMat;
		private Material flashMat;

		private Light flash;
		private HDAdditionalLightData lightData;

		private DetectionSystem detectionSystem;

		private bool isTakingPicture = false;
		private float flashIntensity;
		private float flashDuration;
		private float pictureTakenAtFlashPercentage;
		private int pictureResolution;
		private int screenResolution;

		private bool flashIsActive = true;
		private float renderDelay = 0.2f;
		private float nextRender = 0;

		private float customCooldown;
		private float currentCustomCooldown;

		public Vector3 pictureSpawnOffset = new Vector3(-1, 0, 0);

		private Vector3 camLocalPos;
		private Quaternion camLocalRot;

		public AudioSource source;
		private bool isOnTheFloor = true;

		public bool HasBattery { get => (insertedBattery.charge > 0.01f); }
		



		public const string pictureFolder = "PolaroidPictures";

		public string[] layermaskRaycast = new string[]
		{
			"Default",
			"Terrain",
			"Room",
			"PlaceabkeShipObjects",
			"DecalStickableSurface",
			"Monster",
			"Player"
		};

		private LayerMask rayCastLayerMask;


		private Texture2D polaroidScreenTex = null;

		public override void Start()
		{
			base.Start();
			
			source = GetComponent<AudioSource>();
			flash = GetComponentInChildren<Light>(true);
			lightData = flash.gameObject.GetComponent<HDAdditionalLightData>();
			flash.enabled = false;
			cam = GetComponentInChildren<Camera>(true);
			cam.depth = -10;
			cam.enabled = false;
			cam.cullingMask = cam.cullingMask | LayerMask.GetMask("DecalStickableSurface");
			rayCastLayerMask = LayerMask.GetMask(layermaskRaycast);

			detectionSystem = new DetectionSystem(rayCastLayerMask, cam);

			foreach (Transform t in transform)
			{
				if(t.name == "PolaroidScreenPlane")
				{
					screen = t.gameObject.GetComponent<MeshRenderer>();
					screenMat = screen.material;
					SetScreenColor(Color.black);
				}
				if (t.name == "Mesh")
				{
					var meshRend = t.gameObject.GetComponent<MeshRenderer>();
					flashMat = meshRend.materials[2];
					flashMat.color = Color.green;
					flashMat.SetColor("_EmissiveColor", Color.green);
				}
				if(t.name == "PolaroidNoBatteryPlane")
				{
					noBatteryRend = t.gameObject.GetComponent<MeshRenderer>();
				}
			}	

			if(IsHost || IsServer)
			{
				insertedBattery.charge = 1;
				SyncBatteryServerRpc(100);
			}
			UpdateConfigValue();

			polaroidScreenTex = new Texture2D(cam.targetTexture.width, cam.targetTexture.height);

			camLocalPos = cam.transform.localPosition;
			camLocalRot = cam.transform.localRotation;
		}

		private void SetScreenColor(Color col)
		{
			screenMat.color = col;
			screenMat.SetColor("_EmissiveColor", col);
			screenMat.mainTexture = null;
			screenMat.SetTexture("_EmissiveColorMap", null);
		}

		public override void GrabItem()
		{
			base.GrabItem();
			if (HasBattery)
			{
				source.PlayOneShot(Plugin.AudioPolaroidGrab, 1);
			}
			StartCoroutine(StartScreenRoutine());
			isOnTheFloor = false;
		}
		private bool isScreenStarting = false;
		private IEnumerator StartScreenRoutine()
		{
			float t = 0;
			isScreenStarting = true;
			SetScreenColor(Color.black);	
			yield return new WaitForSeconds(0.1f);
			SetScreenColor(Color.grey);
			yield return new WaitForSeconds(0.1f);
			SetScreenColor(Color.black);
			yield return new WaitForSeconds(0.2f);
			t = 0;
			while( t< 0.2f)
			{
				SetScreenColor(Color.Lerp(Color.white,Color.black,t/0.2f));
				t += Time.deltaTime;
				yield return null;

			}
			isScreenStarting = false;
		}

		public override void EquipItem()
		{
			base.EquipItem();
			playerHeldBy.equippedUsableItemQE = true;
			if (HasBattery)
			{
				if (!isOnTheFloor)
				{
					source.PlayOneShot(Plugin.AudioPolaroidEquip, 1);
				}
			}
		}

		public override void PocketItem()
		{
			if (base.IsOwner && playerHeldBy != null)
			{
				playerHeldBy.equippedUsableItemQE = false;
				isBeingUsed = false;
				SetScreenColor(Color.black);
			}
			
			base.PocketItem();
			noBatteryRend.enabled = false;
		}		

		public override void DiscardItem()
		{
			if (playerHeldBy != null)
			{
				playerHeldBy.equippedUsableItemQE = false;
			}
			isBeingUsed = false;
			SetScreenColor(Color.black);
			base.DiscardItem();
			isOnTheFloor = true;
		}

		public override void OnHitGround()
		{
			base.OnHitGround();
			source.PlayOneShot(Plugin.AudioPolaroidDrop, 1);
		}

		private void UpdateConfigValue()
		{
			Plugin.MyLogger.LogInfo("Apply config values to polaroid");
			cam.fieldOfView = CustomConfig.cameraFov.Value;
			lightData.lightAngle = CustomConfig.flashAngle.Value;
			lightData.range = CustomConfig.flashRange.Value;
			flashIntensity = CustomConfig.flashIntensity.Value;
			flashDuration = CustomConfig.flashDuration.Value;
			pictureTakenAtFlashPercentage = CustomConfig.pictureTakenAtFlashPercentage.Value;
	
			pictureResolution = CustomConfig.pictureResolution.Value;
			screenResolution = CustomConfig.screenResolution.Value;
			SetRenderQuality(false);

			cam.farClipPlane = CustomConfig.polaroidCameraFarClipping.Value;


			customCooldown = CustomConfig.polaroidUseCooldown.Value;

			itemProperties.batteryUsage = 1.0f/(float)CustomConfig.polaroidMaxPictureFromFullBattery.Value;

			renderDelay = CustomConfig.polaroidScreenUpdateRate.Value;
			if (!CustomConfig.polaroidCanUpdateScreen.Value)
			{
				renderDelay = Mathf.Infinity;
			}
		}

		private void SetRenderQuality(bool highQuality)
		{
			if (highQuality)
			{
				cam.targetTexture.Release();
				pictureResolution = CustomConfig.pictureResolution.Value;
				cam.targetTexture.width = pictureResolution;
				cam.targetTexture.height = pictureResolution;
			}
			else
			{
				cam.targetTexture.Release();
				pictureResolution = CustomConfig.pictureResolution.Value;
				cam.targetTexture.width = screenResolution;
				cam.targetTexture.height = screenResolution;
			}
		}

		private bool CanTakePicture()
		{
			if (itemProperties.requiresBattery && (insertedBattery == null || insertedBattery.empty) && !RequireCooldown())
			{
				return false;
			}
			if (insertedBattery.charge <= 0.01f)
			{
				return false;
			}
			return true;
		}

		private int MakePictureID()
		{
			return (int)UnityEngine.Random.Range(1, 99999999);
		}

		public override void ItemInteractLeftRight(bool right)
		{
			base.ItemInteractLeftRight(right);
			if (!right)//"Q"
			{
				if (IsHost || IsServer)
				{
					SetFlashClientRpc(!flashIsActive);

				}
				else
				{
					SetFlashServerRpc(!flashIsActive);
				}
			}		
		}

		[ServerRpc]
		public void SetFlashServerRpc(bool b)
		{
			SetFlashClientRpc(b);
		}

		[ClientRpc]
		public void SetFlashClientRpc(bool b)
		{
			SetFlashLocal(b);
		}

		public void SetFlashLocal(bool b)
		{
			if (b)
			{
				flashMat.color = Color.green;
				flashMat.SetColor("_EmissiveColor", Color.green);
				source.PlayOneShot(Plugin.AudioPolaroidSwitchFlashOn,1);
			}
			else
			{
				flashMat.color = Color.black;
				flashMat.SetColor("_EmissiveColor", Color.black);
				source.PlayOneShot(Plugin.AudioPolaroidSwitchFlashOff,1);
			}

			flashIsActive = b;
			Plugin.MyLogger.LogInfo("Set Flash to : " + b);
		}

		public override void ItemActivate(bool used, bool buttonDown = true)
		{
			base.ItemActivate(used, buttonDown);

			if(currentCustomCooldown > 0)
			{
				return;
			}

			if (buttonDown)
			{
				if (playerHeldBy != null)
				{
					var f = HUDManager.Instance.totalScrapScanned;

					if (CanTakePicture())
					{
						currentCustomCooldown = customCooldown;
						insertedBattery.charge = Mathf.Clamp(insertedBattery.charge - itemProperties.batteryUsage, 0f, 1f);
						Plugin.MyLogger.LogInfo("Polaroid charge updated to " + insertedBattery.charge *100 + "%");
						int id = MakePictureID();

						int cullingMask = playerHeldBy.gameplayCamera.cullingMask;
						if (IsHost || IsServer)
						{
							
							TakePictureClientRpc(id,cullingMask);
							SyncBatteryClientRpc((int)(insertedBattery.charge * 100f));
						}
						else
						{
							TakePictureServerRpc(id, cullingMask);
							SyncBatteryServerRpc((int)(insertedBattery.charge * 100f));
						}
					}
					else
					{
						Plugin.MyLogger.LogInfo("no battery");
					}
				}
			}
		}

		[ServerRpc]
		public void TakePictureServerRpc(int id, int cullingMask)
		{
			//do picture
			TakePictureClientRpc(id, cullingMask);
		}

		[ClientRpc]
		public void TakePictureClientRpc(int id, int cullingMask)
		{
			UpdateConfigValue();
			PolaroidItem polaroidItem = GetComponent<PolaroidItem>();
			pictureCoroutine = DoPictureSequence(id);
			StopCoroutine(pictureCoroutine);
			StartCoroutine(pictureCoroutine);
		}

		private IEnumerator DoPictureSequence(int pictureID)
		{
			float t = 0;
			flash.enabled = true;
			bool pictureTaken = false;
			isTakingPicture = true;
			screenMat.color = Color.black;
			screenMat.SetColor("_EmissiveColor", Color.black);
			screenMat.mainTexture = null;
			screenMat.SetTexture("_EmissiveColorMap", null);
			lightData.SetIntensity(0);

			if (flashIsActive)
			{
				source.PlayOneShot(Plugin.AudioPolaroidShootFlashOn,1);
			}
			else
			{
				source.PlayOneShot(Plugin.AudioPolaroidShootFlashOff, 1);
			}

			while (t < 1)
			{

				if (flashIsActive)
				{
					lightData.SetIntensity(flashIntensity * (1 - t), LightUnit.Lux);
				}

				if (t > pictureTakenAtFlashPercentage && !pictureTaken)
				{
					cam.enabled = true;
					SetRenderQuality(true);
					yield return null;
					Texture2D tex = new Texture2D(cam.targetTexture.width, cam.targetTexture.height);
					RenderTexture(cam, ref tex);				
					SpawnPicture(tex, pictureID);
					pictureTaken = true;
					SetRenderQuality(false);
					cam.enabled = false;
					
				}
				t += Time.deltaTime / flashDuration;

				yield return null;
			}
			flash.enabled = false;
			isTakingPicture = false;
			yield return null;
		}


		private async void SpawnPicture(Texture2D tex, int pictureID)
		{
			GameObject g = Plugin.Instance.picture.spawnPrefab;
			if (g)
			{
				Plugin.MyLogger.LogInfo("saving picture to disk with id : " + pictureID);
				await SaveTextureToDisk(tex, pictureID + "");
				
				if (IsHost || IsServer)
				{
					
					List<VisibleEntity> visEntities = detectionSystem.GetVisibleEntities(playerHeldBy);
					
					Vector3 pos = transform.position + transform.rotation * pictureSpawnOffset;
					if (playerHeldBy)
					{
						pos = playerHeldBy.transform.position;
					}

					GameObject obj = UnityEngine.Object.Instantiate(g, pos, Quaternion.identity, null);
					
					obj.GetComponentInChildren<MeshRenderer>().material.mainTexture = tex;

					PictureItem picture = obj.GetComponent<PictureItem>();
					picture.pictureID = pictureID;
					picture.pictureMetadataID = VisibleEntity.GetVisibleEntitiesToString(visEntities);
					var isUnique = picture.IsPictureUnique();
					

					string pictureName = PictureHelper.MakePictureDescription(visEntities, isUnique);
					int pictureValue = PictureHelper.ComputePictureValue(visEntities, isUnique);

					
					picture.RegisterPictureMetadata();



					ScanNodeProperties scanNode = obj.GetComponentInChildren<ScanNodeProperties>();
					scanNode.headerText = pictureName;
					picture.SetScrapValue(pictureValue);

					Plugin.MyLogger.LogInfo("Spawning picture prefab :"+ "\nwith picture ID: " + pictureID + "\nWith name: " + pictureName + "\nWith value: " + picture.scrapValue);

					obj.GetComponent<NetworkObject>().Spawn();
				}									
			}
		}			

		public override void Update()
		{
			base.Update();
			currentCustomCooldown = Mathf.Max(currentCustomCooldown - Time.deltaTime , 0);
		}

		public override void LateUpdate()
		{
			base.LateUpdate();
			PolaroidScreenUpdate();

			//Make sure the camera position and rotation is sync
			if (isTakingPicture)
			{
				if (isHeld)
				{

					if (playerHeldBy != null && playerHeldBy.IsOwner) {


						if (IsHost || IsServer)
						{
							//base.LateUpdate();
							cam.transform.localPosition = camLocalPos;
							cam.transform.localRotation = camLocalRot;
							UpdatePosRotClientRpc(cam.transform.position, cam.transform.rotation);
						}
						else
						{						
							cam.transform.localPosition = camLocalPos;
							cam.transform.localRotation = camLocalRot;
							//base.LateUpdate();
							UpdatePosRotServerRpc(cam.transform.position,cam.transform.rotation);
						}
					}				
				}
			}
			else
			{
				cam.transform.localPosition = camLocalPos;
				cam.transform.localRotation = camLocalRot;
			}

			if (!HasBattery && !isPocketed)
			{
				noBatteryRend.enabled = true;
				flashMat.color = Color.black;
				flashMat.SetColor("_EmissiveColor", Color.black);
			}
			else
			{
				noBatteryRend.enabled = false;
				if (flashIsActive)
				{
					flashMat.color = Color.green;
					flashMat.SetColor("_EmissiveColor", Color.green);
				}
				
			}
		}

		[ServerRpc]
		private void UpdatePosRotServerRpc(Vector3 pos, Quaternion rot)
		{
			UpdatePosRotClientRpc(pos, rot);
		}

		[ClientRpc]
		private void UpdatePosRotClientRpc(Vector3 pos, Quaternion rot)
		{
			cam.transform.position = pos;
			cam.transform.rotation = rot;
		}

		private bool isScreenUpdating = false;
		private void PolaroidScreenUpdate()
		{
			if (!isTakingPicture)
			{
				if (CanTakePicture() && isHeld && playerHeldBy!= null && playerHeldBy.IsOwner && !isPocketed && !isScreenStarting)
				{			
					if (nextRender <= 0)
					{
						if(!cam.enabled)cam.enabled = true;
						if (!isScreenUpdating)
						{
							screenMat.color = Color.white;
							screenMat.SetColor("_EmissiveColor", Color.white);
							screenMat.mainTexture = cam.targetTexture;
							screenMat.SetTexture("_EmissiveColorMap", cam.targetTexture);
							isScreenUpdating = true;
						}
						
						nextRender = renderDelay;
					}
					else
					{
						if (renderDelay > 0)
						{
							cam.enabled = false;
							isScreenUpdating = false;
						}
					}
					nextRender -= Time.deltaTime;

				}
				else
				{
					cam.enabled = false;
					isScreenUpdating = false;
				}
			}
			else
			{
				isScreenUpdating = false;
			}
		}

		private Texture2D RenderTexture(Camera camera, ref Texture2D tex)
		{
			var currentRT = UnityEngine.RenderTexture.active;
			UnityEngine.RenderTexture.active = camera.targetTexture;
			tex.ReadPixels(new Rect(0, 0, camera.targetTexture.width, camera.targetTexture.height), 0, 0);
			tex.Apply();
			UnityEngine.RenderTexture.active = currentRT;
			return tex;
		}

		public static string GetDirPath()
		{
			return UnityEngine.Application.dataPath + "/../" + pictureFolder + "/";
		}

		public async Task SaveTextureToDisk(Texture2D tex, string fileName)
		{
			byte[] bytes = tex.EncodeToPNG();
			var dirPath = GetDirPath();
			if (!Directory.Exists(dirPath))
			{
				Directory.CreateDirectory(dirPath);
			}
			string filePath = dirPath + "" + fileName + ".png";
			if (!File.Exists(filePath))
			{
				Plugin.MyLogger.LogInfo("trying to write file at: " + filePath);
				await File.WriteAllBytesAsync(filePath, bytes);
				Plugin.MyLogger.LogInfo("Finishing writing file at : " + filePath);
			}
			else
			{
				Plugin.MyLogger.LogWarning("File already exist !");
			}
			
		}
	}
}