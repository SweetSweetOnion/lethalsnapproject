﻿using GameNetcodeStuff;
using System;
using System.Collections.Generic;
using System.Text;
using Unity.Profiling;
using static UnityEngine.UI.GridLayoutGroup;

namespace LethalSnapProject.Behaviour
{
	static class MonsterReact
	{
		private static void StunMonster(EnemyAI enemy, PlayerControllerB owner, float stunDuration)
		{
			Plugin.MyLogger.LogInfo(enemy.enemyType.name + " is stunned by " + owner.playerUsername);
			enemy.SetEnemyStunned(true, stunDuration, owner);
		}

		private static void TargetOwner(EnemyAI enemy, PlayerControllerB owner)
		{
			Plugin.MyLogger.LogInfo(enemy.enemyType.name + " target " + owner.playerUsername);
			enemy.SetMovingTowardsTargetPlayer(owner);
		}

		private static void AngryJester(EnemyAI enemy, PlayerControllerB owner)
		{
			JesterAI jester = enemy as JesterAI;
			if (jester == null) return;

			Plugin.MyLogger.LogInfo("Jester pop !");
			jester.beginCrankingTimer = 0;
			jester.popUpTimer = 0;
		}

		private static void AngryFlowerman(EnemyAI enemy, PlayerControllerB owner)
		{
			FlowermanAI flowerman = enemy as FlowermanAI;
			if (flowerman == null) return;
			flowerman.SwitchToBehaviourStateOnLocalClient(2);
			flowerman.EnterAngerModeServerRpc(100f);
		}

		public static bool ChanceOfTrigger(float chance)
		{
			if(UnityEngine.Random.value <= chance)
			{
				return true;
			}
			Plugin.MyLogger.LogWarning("Lucky! the monster didn't react to the flash (this time)");
			return false;
		}


		public static void ReactToFlash(PlayerControllerB owner, EnemyAI enemy, float distance, bool isInPicture)
		{
			if(!owner || !enemy)
			{
				Plugin.MyLogger.LogError("No owner or enemy ! Enemy can't react");
				return;
			}

			enemy.DetectNoise(owner.transform.position, 1.5f,1,0);

			string lowerMonsterName = enemy.enemyType.enemyName.ToLower();
			switch (lowerMonsterName)
			{
				case "flowerman":
					if(ChanceOfTrigger(0.5f))AngryFlowerman(enemy, owner);
					break;
				case "crawler":
					TargetOwner(enemy, owner);
					break;
				case "hoarding bug":
					StunMonster(enemy, owner,10f);
					break;
				case "centipede":
					TargetOwner(enemy, owner);
					break;
				case "bunker spider":
					TargetOwner(enemy, owner);
					break;
				case "puffer":
					TargetOwner(enemy, owner);
					break;
				case "jester":
					if (ChanceOfTrigger(0.33f)) AngryJester(enemy, owner);
					break;
				case "blob":
					TargetOwner(enemy, owner);
					break;
				case "girl":
					TargetOwner(enemy, owner);
					break;
				case "spring":
					TargetOwner(enemy, owner);
					break;
				case "nutcracker":
					TargetOwner(enemy, owner);
					break;
				case "masked":
					TargetOwner(enemy, owner);
					break;
				case "mouthdog":
					TargetOwner(enemy, owner);
					break;
				case "earth leviathan":
					TargetOwner(enemy, owner);
					break;
				case "forestgiant":
					TargetOwner(enemy, owner);
					break;
				case "baboon hawk":
					TargetOwner(enemy, owner);
					break;
				case "red locust bees":
					TargetOwner(enemy, owner);
					break;
				case "docile locust bees":
					TargetOwner(enemy, owner);
					break;
				case "manticoil":
					TargetOwner(enemy, owner);
					break;
				default:
					Plugin.MyLogger.LogError("No special behaviour for " + lowerMonsterName);
					break;
			}
		}
	}
}
