﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

[Serializable]
public struct VisibleEntity
{
	public bool isPlayer;
	public string name;
	public bool isOutside;
	public ulong networkID;

	public static string ToMetadataString(List<VisibleEntity> entities)
	{
		BinaryFormatter bf = new BinaryFormatter();
		MemoryStream output = new MemoryStream();
		bf.Serialize(output, entities);
		return Convert.ToBase64String(output.GetBuffer());
	}

	public static string GetVisibleEntitiesToString(List<VisibleEntity> entities)
	{
		string str = "";
		foreach(VisibleEntity entity in entities)
		{
			str += entity.name;
		}
		return str;
	}
}