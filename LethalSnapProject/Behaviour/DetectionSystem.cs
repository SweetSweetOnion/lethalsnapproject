﻿using GameNetcodeStuff;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace LethalSnapProject.Behaviour
{
	internal class DetectionSystem
	{
		private LayerMask _layerMask;
		private Camera _camera;
		public float detectionDistance = 50;

		public DetectionSystem(LayerMask layerMask, Camera camera)
		{
			_layerMask = layerMask;
			_camera = camera;
		}

		public List<VisibleEntity> GetVisibleEntities(PlayerControllerB owner)
		{
			List<VisibleEntity> visibles = new List<VisibleEntity>();
			Helper.PrintLayerMask(_layerMask, "Raycast layer");
			//Detect players
			PlayerControllerB[] players = MonoBehaviour.FindObjectsOfType<PlayerControllerB>(false);
			Vector3[] playersPoint = new Vector3[]
			{
				new Vector3(0, 2.25f, 0),
				new Vector3(0, 2f, 0),
				new Vector3(0, 1.75f, 0),
				new Vector3(0, 1.5f, 0),
				new Vector3(0, 1.25f, 0),
				new Vector3(0, 1f, 0),
				new Vector3(0, 0.75f, 0),
				new Vector3(0, 0.5f, 0),
				new Vector3(0, 0.25f, 0),
				new Vector3(0, 0, 0),

			};

			foreach (PlayerControllerB player in players)
			{
				if (player.isFreeCamera) continue;
				if (!player.isPlayerControlled) continue;

				foreach (Vector3 point in playersPoint)
				{
					Vector3 p = player.transform.position + point;
					
					if (IsInViewPort(p))
					{
						float distance = Vector3.Distance(p, _camera.transform.position);
						if (Plugin.DebugMode)Plugin.MyLogger.LogWarning("Is in viewPort");
						if (IsInRange(p,distance))
						{
							if (Plugin.DebugMode) Plugin.MyLogger.LogWarning("Is in range");
							if (IsRayCastVisible(p,player.gameObject))
							{
								VisibleEntity visibleEntity = new VisibleEntity()
								{
									name = player.playerUsername,
									isPlayer = true,
									networkID = player.NetworkObjectId
								};
								visibles.Add(visibleEntity);
								break;
							}
						}
					}
				}
			}


			//Detect enemy 
			EnemyAI[] enemies = MonoBehaviour.FindObjectsOfType<EnemyAI>(false);
			foreach (EnemyAI enemy in enemies)
			{
				foreach (Vector3 point in playersPoint)
				{
					Vector3 p = enemy.transform.position + point;
					
					if (IsInViewPort(p))
					{
						float distance = Vector3.Distance(p, _camera.transform.position);
						if (Plugin.DebugMode) Plugin.MyLogger.LogWarning("Is in viewPort");
						if (IsInRange(p,distance))
						{
							if (Plugin.DebugMode) Plugin.MyLogger.LogWarning("Is in range");
							if (IsRayCastVisible(p,enemy.gameObject))
							{
								var earthWorm = enemy as SandWormAI;
								if (earthWorm && !earthWorm.emerged)
								{
									break;
								}

								VisibleEntity visibleEntity = new VisibleEntity()
								{
									name = enemy.enemyType.enemyName,
									isPlayer = false,
									isOutside = enemy.isOutside,
									networkID = enemy.NetworkObjectId
								};
								visibles.Add(visibleEntity);

								if (CustomConfig.monsterReactToFlash.Value)
								{
									if (owner)
									{
										MonsterReact.ReactToFlash(owner, enemy, distance, true);
									}
									
								}

								break;
							}
						}
					}
				}
			}
			return visibles;
		}

		private bool IsInRange(Vector3 p, float distance)
		{
			if (distance > detectionDistance)
			{
				return false;
			}
			return true;
		}

		private bool IsInViewPort(Vector3 p)
		{
			Vector3 vwp = _camera.WorldToViewportPoint(p);
			if (vwp.z > 0 && vwp.x > 0 && vwp.x < 1 && vwp.y > 0 && vwp.y < 1)
			{
				return true;
			}
			return false;
		}

		private bool IsRayCastVisible(Vector3 p, GameObject target)
		{
			RaycastHit hit;
			float d = Vector3.Distance(_camera.transform.position, p);
			if (Physics.Raycast(_camera.transform.position, p - _camera.transform.position, out hit, d, _layerMask))
			{
				if(hit.transform.gameObject == target)
				{
					if (Plugin.DebugMode)
					{
						Plugin.MyLogger.LogWarning("Hit target");
						Helper.SpawnDebugVisual(hit.point, Color.green);
					}
					return true;
				}

				if (Plugin.DebugMode)
				{
					Plugin.MyLogger.LogWarning("SOMETHING IS BLOCKING : " + hit.transform.name + "  " + LayerMask.LayerToName(hit.transform.gameObject.layer));
					Helper.SpawnDebugVisual(hit.point, Color.red);
				}
				return false;

			}
			if (Plugin.DebugMode)
			{
				Helper.SpawnDebugVisual(p, Color.green);
			}
			return true;
		}
	}
}
