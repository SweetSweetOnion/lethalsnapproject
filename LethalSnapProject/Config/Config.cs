﻿using BepInEx.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using Unity.Collections;
using Unity.Netcode;
using LethalSnapProject;
using GameNetcodeStuff;
using HarmonyLib;
using LethalSnapProject.Behaviour;

[Serializable]
public class CustomConfig : SyncedInstance<CustomConfig>
{
	public static ConfigEntry<bool> debugMode;
	//Gameplay
	public static ConfigEntry<string> monsterValues;
	public static ConfigEntry<int> defaultMonsterValue;
	public static ConfigEntry<int> monsterValueMultiplier;
	public static ConfigEntry<float> pictureScaleMultiplier;
	public static ConfigEntry<bool> monsterReactToFlash;
	public static ConfigEntry<int> polaroidMaxPictureFromFullBattery;
	public static ConfigEntry<int> polaroidLootRarity;
	public static ConfigEntry<bool> polaroidCanBeBuy;
	public static ConfigEntry<int> polaroidCost;
	public static ConfigEntry<bool> canPinPictureToPlayers;
	
	//Flash
	public static ConfigEntry<float> cameraFov;
	public static ConfigEntry<float> flashAngle;
	public static ConfigEntry<float> flashIntensity;
	public static ConfigEntry<float> flashRange;
	public static ConfigEntry<float> flashDuration;
	public static ConfigEntry<float> pictureTakenAtFlashPercentage;
	//Polaroid
	public static ConfigEntry<int> pictureResolution;	
	public static ConfigEntry<int> screenResolution;
	public static ConfigEntry<bool> polaroidCanUpdateScreen;
	public static ConfigEntry<float> polaroidScreenUpdateRate;
	public static ConfigEntry<float> polaroidCameraFarClipping;
	public static ConfigEntry<float> polaroidUseCooldown;
	
	public CustomConfig(ConfigFile cfg)
	{
		InitInstance(this);

		debugMode = cfg.Bind(
			"Debug",
			"debugMode",
			false,
			"More log"
		);
		//Gameplay	
		monsterValues = cfg.Bind(
			"Gameplay",
			"Monsters value",
			"" + MakeMonsterString(),
			"Assign a monster name to a value monsterName1:scrapValue1,monsterName2:scrapValue2,... (Can be used to overide base monster values too)"
			);

		defaultMonsterValue = cfg.Bind(
			"Gameplay",
			"Default monster value",
			10,
			"Change default value for monster not listed above (MonsterValues)"
			);

		monsterValueMultiplier = cfg.Bind(
			"Gameplay",
			"Monster value multiplier",
			1,
			"Change this value to multiply the value of all pictures"
			);

		monsterReactToFlash = cfg.Bind(
			"Gameplay",
			"Monster React to flash",
			true,
			"Does monster have special reaction when takken in picture"
			);

		pictureScaleMultiplier = cfg.Bind(
			"Gameplay",
			"PictureScaleMultiplier",
			1f,
			"Make the picture bigger"
			);

		polaroidMaxPictureFromFullBattery = cfg.Bind(
			"Gameplay",
			"PolaroidMaxPictureFromFullBattery",
			5,
			"The number of picture which can be taken before running out of battery"
			);

		polaroidLootRarity = cfg.Bind(
			"Gameplay",
			"PolaroidLootRarity",
			5,
			"Chance of looting a polaroid (1 very rare, 100 very common)"
			);

		polaroidCanBeBuy = cfg.Bind(
			"Gameplay",
			"PolaroidCanBeBuy",
			true,
			"Does the polaroid appear in the store ?"
			);

		polaroidCost = cfg.Bind(
			"Gameplay",
			"PolaroidCost",
			150,
			"The cost of the polaroid in the store"
			);

		canPinPictureToPlayers = cfg.Bind(
			"Gameplay",
			"canPinPictureToPlayers",
			true,
			"Does the picture can be pin directly to another players ? "
			);

		//Flash
		cameraFov = cfg.Bind(
			"Flash",
			"CameraFov",
			75f,
			"The Field of view of the polaroid"
			);

		flashAngle = cfg.Bind(
			"Flash",
			"FlashAngle",
			160f,
			"The wideness of the flash (in degree)"
			);

		flashIntensity = cfg.Bind(
			"Flash",
			"FlashIntensity",
			250f,
			"The intensity of the flash"
			);

		flashRange = cfg.Bind(
			"Flash",
			"FlashRange",
			50f,
			"The range of the flash in meter"
			);

		flashDuration = cfg.Bind(
			"Flash",
			"FlashDuration",
			0.8f,
			"The duration of the flash in seconds (How long the intensity goes back to 0)"
			);

		pictureTakenAtFlashPercentage = cfg.Bind(
			"Flash",
			"PictureTakenAtFlashPercentage",
			0.5f,
			"At which percentage of the flash duration the picture is actually taken (the intensity when the picture is taken is : flashIntensity / (1-pictureTakenAtFlashPercentage) )"
			);

		//polaroid
		pictureResolution = cfg.Bind(
			"Polaroid",
			"PictureResolution",
			512,
			"The resolution of the pic 128,256,512,1024... (careful can heavily impact performance when taking picture)"
			);

		screenResolution = cfg.Bind(
			"Polaroid",
			"ScreenResolution",
			64,
			"The resolution of the polaroid screen 128,256,512,1024... (careful can heavily impact performance when the polaroid is held)"
			);

		polaroidCanUpdateScreen = cfg.Bind(
			"Polaroid",
			"PolaroidCanUpdateScreen",
			true,
			"Does the polaroid preview the picture in its screen ? (disable it to improve performance)"
			);

		polaroidScreenUpdateRate = cfg.Bind(
			"Polaroid",
			"PolaroidScreenUpdateRate",
			0.0f,
			"The delay betway polaroid screen update in seconds (increase this if you have performance issue)"
			);

		polaroidCameraFarClipping = cfg.Bind(
			"Polaroid",
			"PolaroidCameraFarClipping",
			100.0f,
			"How far the polaroid camera can render (reduce this if you have performance issue)"
			);

		polaroidUseCooldown = cfg.Bind(
			"Polaroid",
			"PolaroidUseCooldown",
			1f,
			"The required cooldown between 2 pictures"
			);
	}

	private string MakeMonsterString()
	{
		string str = "";

		str += "flowerman:180,";
		str += "crawler:90,";
		str += "hoarding bug:2,";
		str += "centipede:10,";
		str += "bunker spider:50,";
		str += "puffer:10,";
		str += "jester:250,";
		str += "blob:2,";
		str += "girl:180,";
		str += "spring:30,";
		str += "nutcracker:80,";
		str += "masked:60,";
		str += "mouthdog:40,";
		str += "earth leviathan:120,";
		str += "forestgiant:40,";
		str += "baboon hawk:10,";
		str += "red locust bees:2,";
		str += "docile locust bees:0,";
		str += "manticoil:0,";
		str += "peeper:2,";
		str += "locker:60,";
		str += "fiend:80";
 
		return str;
	}

	public static void RequestSync()
	{
		if (!IsClient) return;

		using FastBufferWriter stream = new(IntSize, Allocator.Temp);
		MessageManager.SendNamedMessage("LethalSnap_OnRequestConfigSync", 0uL, stream);
	}

	public static void OnRequestSync(ulong clientId, FastBufferReader _)
	{
		if (!IsHost) return;

		Plugin.MyLogger.LogInfo($"Config sync request received from client: {clientId}");

		byte[] array = SerializeToBytes(Instance);
		int value = array.Length;

		using FastBufferWriter stream = new(value + IntSize, Allocator.Temp);

		try
		{
			stream.WriteValueSafe(in value, default);
			stream.WriteBytesSafe(array);

			MessageManager.SendNamedMessage("LethalSnap_OnReceiveConfigSync", clientId, stream);
		}
		catch (Exception e)
		{
			Plugin.MyLogger.LogInfo($"Error occurred syncing config with client: {clientId}\n{e}");
		}
	}

	public static void OnReceiveSync(ulong _, FastBufferReader reader)
	{
		if (!reader.TryBeginRead(IntSize))
		{
			Plugin.MyLogger.LogError("Config sync error: Could not begin reading buffer.");
			return;
		}

		reader.ReadValueSafe(out int val, default);
		if (!reader.TryBeginRead(val))
		{
			Plugin.MyLogger.LogError("Config sync error: Host could not sync.");
			return;
		}

		byte[] data = new byte[val];
		reader.ReadBytesSafe(ref data, val);

		SyncInstance(data);

		Plugin.MyLogger.LogInfo("Successfully synced config with host.");
	}


	[HarmonyPostfix]
	[HarmonyPatch(typeof(GameNetworkManager), "SteamMatchmaking_OnLobbyMemberJoined")]
	public static void InitializeLocalPlayer()
	{
		Plugin.MyLogger.LogInfo("Trying to sync config files");
		if (IsHost)
		{
			MessageManager.RegisterNamedMessageHandler("LethalSnap_OnRequestConfigSync", OnRequestSync);
			Synced = true;

			return;
		}

		Synced = false;
		MessageManager.RegisterNamedMessageHandler("LethalSnap_OnReceiveConfigSync", OnReceiveSync);
		RequestSync();
	}

	[HarmonyPostfix]
	[HarmonyPatch(typeof(GameNetworkManager), "StartDisconnect")]
	public static void PlayerLeave()
	{
		Plugin.MyLogger.LogInfo("Trying to reset config file");
		CustomConfig.RevertSync();
	}
}

