﻿using HarmonyLib;
using System.Collections.Generic;
using System.Text;
using Unity.Netcode;
using UnityEngine;

namespace LethalSnapProject.Patcher
{
	[HarmonyPatch]
	internal class NetworkObjectManager
	{
		private static GameObject polaroidNetworkPrefab;
		private static GameObject pictureNetworkPrefab;

		[HarmonyPostfix]
		[HarmonyPatch(typeof(GameNetworkManager), "Start")]
		public static void Init()
		{
			if (polaroidNetworkPrefab == null)
			{
				polaroidNetworkPrefab = Plugin.Instance.polaroidPrefab;
				NetworkManager.Singleton.AddNetworkPrefab(Plugin.Instance.polaroidPrefab);
			}
			if (pictureNetworkPrefab == null)
			{
				pictureNetworkPrefab = Plugin.Instance.picturePrefab;
				NetworkManager.Singleton.AddNetworkPrefab(Plugin.Instance.picturePrefab);
			}
		}

		[HarmonyPostfix]
		[HarmonyPatch(typeof(StartOfRound), "Awake")]
		private static void SpawnNetworkHandler()
		{ 
			if (NetworkManager.Singleton.IsHost || NetworkManager.Singleton.IsServer)
			{
				GameObject polaroid = Object.Instantiate(polaroidNetworkPrefab, new Vector3(0f, -1500f, 0f), Quaternion.identity);
				polaroid.GetComponent<NetworkObject>().Spawn(false);
				polaroid.SetActive(false);

				GameObject picture = Object.Instantiate(pictureNetworkPrefab, new Vector3(0f, -1500f, 0f), Quaternion.identity);
				picture.GetComponent<NetworkObject>().Spawn(false);
				picture.SetActive(false);

			}
		}
	}
}
