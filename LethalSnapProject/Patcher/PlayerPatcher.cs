﻿using GameNetcodeStuff;
using HarmonyLib;
using UnityEngine.Rendering;
using UnityEngine;

[HarmonyPatch(typeof(PlayerControllerB), "SpawnPlayerAnimation")]
internal class PlayerPatch
{
	private static void Postfix(ref PlayerControllerB __instance)
	{
		if ((Object)(object)__instance == (Object)(object)GameNetworkManager.Instance.localPlayerController)
		{
			PlayerControllerB obj = __instance;
			__instance.GetComponentInChildren<LODGroup>().enabled = false;
			obj.thisPlayerModel.shadowCastingMode = (ShadowCastingMode)1;
			obj.localVisor.GetComponentInChildren<Renderer>().shadowCastingMode = (ShadowCastingMode)0;
			obj.thisPlayerModelLOD1.shadowCastingMode = (ShadowCastingMode)3;
			obj.thisPlayerModelLOD2.enabled = false;
			obj.thisPlayerModel.gameObject.layer = 23;
			obj.thisPlayerModelArms.gameObject.layer = 5;
		}
	}
}