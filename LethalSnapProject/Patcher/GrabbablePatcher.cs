﻿using GameNetcodeStuff;
using HarmonyLib;
using LethalSnapProject.Behaviour;
using System;
using System.Collections.Generic;
using System.Text;
using Unity.Netcode;
using UnityEngine;

namespace LethalSnapProject.Patcher
{
	[HarmonyPatch(typeof(GrabbableObject), "UseItemOnClient")]
	internal class GrabbablePatcher
	{
		private static void Postfix(ref GrabbableObject __instance)
		{
			GrabbableObject grab = __instance;
			PolaroidItem item = grab as PolaroidItem;
			if (item)
			{
				if (item.insertedBattery.charge == 0)
				{
					item.source.PlayOneShot(Plugin.AudioPolaroidShootEmpty, 1);
				}
			}
		}
	}
}
