﻿using LethalLib;
using UnityEngine;


public static class Helper
{
	public static void PlayOneShot(this AudioSource source, AudioClip[] array, float volume = 1)
	{
		int r = Random.Range(0, array.Length);
		source.PlayOneShot(array[r], volume);
	}

	public static string GetMountPath(Transform t)
	{
		string str = "";
		Transform cur = t.parent;

		if (cur == null) return "";

		while (cur.parent != null)
		{
			str = cur.name + "/" + str;
			cur = cur.parent;
		}
		return str.Trim("/".ToCharArray());
	}

	public static string FindFirstTransformFromMountPath(string mountPath)
	{
		if (mountPath == "") return "";
		return mountPath.Split("/")[0];
	}

	public static Transform FindTransformFromMountPath(string mountPath)
	{
		if (mountPath == "") return null;
		string[] names = mountPath.Split("/");
		Transform root = GameObject.Find(names[0]).transform;
		Transform parent = root;
		for (int i = 1; i < names.Length; i++)
		{
			parent = parent.Find(names[i]);
		}
		return parent;
	}

	public static void PrintLayerMask(LayerMask layerMask, string layerMaskName)
	{
		Plugin.logger.LogInfo("||||| printing layers of layermask " + layerMaskName);
		for(int i =0; i<31; i++)
		{
			if (IsInLayerMask(i, layerMask))
			{
				Plugin.logger.LogInfo("-> " + i + " " + LayerMask.LayerToName(i));
			}
		}
		Plugin.logger.LogInfo("||||| End of : " + layerMaskName);
	}

	public static bool IsInLayerMask(int layer, LayerMask layermask)
	{
		return layermask == (layermask | (1 << layer));
	}

	public static GameObject SpawnDebugVisual(Vector3 pos, Color c)
	{
		GameObject g = MonoBehaviour.FindObjectOfType<UnlockableSuit>().gameObject;
		GameObject v = g.transform.Find("SuitHook").gameObject;	
		GameObject n = MonoBehaviour.Instantiate(v, pos, Quaternion.identity, null);

		Material mat = n.GetComponent<MeshRenderer>().material;
		mat.color = c;
		mat.SetColor("_EmissiveColor", c);
		mat.mainTexture = null;
		mat.SetTexture("_EmissiveColorMap", null);

		return n;
	}
}

