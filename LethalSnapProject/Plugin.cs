﻿using BepInEx;
using UnityEngine;
using System.Reflection;
using HarmonyLib;
using LethalLib.Modules;
using System.IO;
using LethalSnapProject.Behaviour;
using BepInEx.Logging;
using System.Drawing;
using UnityEngine.Assertions;
using System;

namespace LethalSnapProject
{
	[BepInPlugin(PluginInfo.PLUGIN_GUID, PluginInfo.PLUGIN_NAME, PluginInfo.PLUGIN_VERSION)]
	public class Plugin : BaseUnityPlugin
	{
		private readonly Harmony harmony = new Harmony(PluginInfo.PLUGIN_GUID);

		public static Plugin Instance;
		public Item picture;

		public GameObject polaroidPrefab;
		public GameObject picturePrefab;

		public static ManualLogSource MyLogger { get; internal set; }

		public static new CustomConfig MyConfig { get; internal set; }

		public static AudioClip[] AudioPolaroidDrop;
		public static AudioClip[] AudioPolaroidEquip;
		public static AudioClip[] AudioPolaroidGrab;
		
		public static AudioClip[] AudioPictureGrab;
		public static AudioClip[] AudioPictureEquip;
		public static AudioClip[] AudioPictureDrop;

		public static AudioClip[] AudioPolaroidShootEmpty;
		public static AudioClip[] AudioPolaroidShootFlashOff;
		public static AudioClip[] AudioPolaroidShootFlashOn;

		public static AudioClip[] AudioPolaroidSwitchFlashOff;
		public static AudioClip[] AudioPolaroidSwitchFlashOn;

		public static bool DebugMode;


		private void Awake()
		{
			

			if (Instance == null)
			{
				Instance = this;
			}

			MyConfig = new(base.Config);

			DebugMode = CustomConfig.debugMode.Value;

			MyLogger = BepInEx.Logging.Logger.CreateLogSource(PluginInfo.PLUGIN_GUID);

			//Load asset bundle
			string assetDir = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "polaroiditemmod");
			AssetBundle bundle = AssetBundle.LoadFromFile(assetDir);

			//Make polaroid item
			Item polaroid = bundle.LoadAsset<Item>("Assets/PolaroidMod/PolaroidItem.asset");
			polaroid.requiresBattery = true;//battery usage set via polaroid script
			PolaroidItem polaroidScript = polaroid.spawnPrefab.AddComponent<PolaroidItem>();
			polaroidScript.grabbable = true;
			polaroidScript.grabbableToEnemies = true;
			polaroidScript.itemProperties = polaroid;
			polaroidPrefab = polaroid.spawnPrefab;
			Items.RegisterScrap(polaroid, CustomConfig.polaroidLootRarity.Value , Levels.LevelTypes.All);
			
			//audio stuff
			Utilities.FixMixerGroups(polaroid.spawnPrefab);

			if (CustomConfig.polaroidCanBeBuy.Value)
			{
				//Add polaroid into terminal
				TerminalNode node = ScriptableObject.CreateInstance<TerminalNode>();
				node.clearPreviousText = true;
				node.displayText =
					"Take picture of monsters.\n" +
					"Sell them to the Company.\n" +
					"Live for another day of work\n\n" +
					"Each monster give different value to the pictures (based on their dangerosity)\n" +
					"Friend in the picture with monsters bring more value to it\n" +
					"You can take " + CustomConfig.polaroidMaxPictureFromFullBattery.Value + " pictures before running out of battery (can be charge in the ship)\n";
				Items.RegisterShopItem(polaroid, null, null, node, CustomConfig.polaroidCost.Value);
			}		

			//Make picture item
			picture = bundle.LoadAsset<Item>("Assets/PolaroidMod/PictureItem.asset");
			PictureItem pictureScript = picture.spawnPrefab.AddComponent<PictureItem>();	
			pictureScript.grabbable = true;
			pictureScript.grabbableToEnemies = true;
			pictureScript.itemProperties = picture;
			picturePrefab = picture.spawnPrefab;
			Items.RegisterItem(picture);
			//audio stuff
			Utilities.FixMixerGroups(picture.spawnPrefab);



			//Network stuff
			var types = Assembly.GetExecutingAssembly().GetTypes();
			foreach (var type in types)
			{
				var methods = type.GetMethods(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
				foreach (var method in methods)
				{
					var attributes = method.GetCustomAttributes(typeof(RuntimeInitializeOnLoadMethodAttribute), false);
					if (attributes.Length > 0)
					{
						method.Invoke(null, null);
					}
				}
			}

			//Audio stuff
			AudioPolaroidDrop = LoadAudio(bundle, "Assets/PolaroidMod/Audio/FOLEY_LS_POL_Drop/FOLEY_LS_POL_Drop_", 8);
			AudioPolaroidEquip = LoadAudio(bundle, "Assets/PolaroidMod/Audio/FOLEY_LS_POL_Equip/FOLEY_LS_POL_Equip_", 8);
			AudioPolaroidGrab = LoadAudio(bundle, "Assets/PolaroidMod/Audio/FOLEY_LS_POL_Grab/FOLEY_LS_POL_Grab_", 8);

			AudioPictureGrab = LoadAudio(bundle, "Assets/PolaroidMod/Audio/FOLEY_LS_POL_Picture_Grab/FOLEY_LS_POL_Picture_Grab_", 10);
			AudioPictureEquip = LoadAudio(bundle, "Assets/PolaroidMod/Audio/FOLEY_LS_POL_Picture_Equip/FOLEY_LS_POL_Picture_Equip_", 10);
			AudioPictureDrop = LoadAudio(bundle, "Assets/PolaroidMod/Audio/FOLEY_LS_POL_Picture_Drop/FOLEY_LS_POL_Picture_Drop_", 10);

			AudioPolaroidShootEmpty = LoadAudio(bundle, "Assets/PolaroidMod/Audio/SFX_LS_POL_Shoot_Empty/SFX_LS_POL_Shoot_Empty_", 6);
			AudioPolaroidShootFlashOff = LoadAudio(bundle, "Assets/PolaroidMod/Audio/SFX_LS_POL_Shoot_Flash_Off/SFX_LS_POL_Shoot_Flash_Off_", 6);
			AudioPolaroidShootFlashOn = LoadAudio(bundle, "Assets/PolaroidMod/Audio/SFX_LS_POL_Shoot_Flash_On/SFX_LS_POL_Shoot_Flash_On_", 6);

			AudioPolaroidSwitchFlashOff = LoadAudio(bundle, "Assets/PolaroidMod/Audio/SFX_LS_POL_Switch_Flash_Off/SFX_LS_POL_Switch_Flash_Off_", 8);
			AudioPolaroidSwitchFlashOn = LoadAudio(bundle, "Assets/PolaroidMod/Audio/SFX_LS_POL_Switch_Flash_On/SFX_LS_POL_Switch_Flash_On_", 8);

			// Plugin startup logic
			Logger.LogInfo($"Plugin {PluginInfo.PLUGIN_GUID} is loaded!");
			harmony.PatchAll();


			string monsters = CustomConfig.monsterValues.Value;

			string[] monsterValuePair = monsters.Split(",");

			Plugin.MyLogger.LogInfo("Display monsters and there values : ");
			foreach (string mvp in monsterValuePair)
			{
				string[] m = mvp.Split(":");
				if(m.Length == 2)
				{
					try
					{
						int value = Int32.Parse(m[1]);
						var p = new PictureHelper.MonsterNameValue(m[0].ToLower(), value);
						PictureHelper.additionalMonsterValues.Add(p);
						Plugin.MyLogger.LogInfo("--> " + p.Name + "  " + p.Value);
					}
					catch (FormatException)
					{
						Plugin.MyLogger.LogError("Add monster config error! Scrap value isn't a number! ");
					}
				}
				else
				{
					Plugin.MyLogger.LogError("Error in config files ! Can't read entry: " + mvp + " (don't add \'|\' at the end)");
				}			
			}

		}
		public AudioClip[] LoadAudio(AssetBundle bundle, string assetPath, int count)
		{
			AudioClip[] clips = new AudioClip[count];
			for(int i =1; i<= count; i++)
			{
				var str = assetPath;
				if(i >= 10)
				{
					str += i + ".wav";
				}
				else
				{
					str += "0" + i + ".wav";
				}
				
				clips[i-1] = bundle.LoadAsset<AudioClip>(str);
				if(clips[i-1] == null)
				{
					Plugin.MyLogger.LogError("Can't load :" + assetPath);
				}
			}
			Plugin.MyLogger.LogInfo("Finish Loading : " + assetPath);
			return clips;
		}

}
}
